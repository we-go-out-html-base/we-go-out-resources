function extractSpotifyCode() {
    var inputField = document.getElementById('input-spotify-Field');
    var resultContainer = document.getElementById('result-spotify-Container');
    var errorDiv = document.getElementById('error-spotify');

    var spotifyCode = inputField.value.trim();

    if (spotifyCode === '') {
        errorDiv.textContent = 'Por favor, insira um código do iframe do Spotify.';
        resultContainer.textContent = '';
        return;
    }

    errorDiv.textContent = '';

    try {
        // Extrair a URL do iframe do Spotify
        var url = spotifyCode.match(/src="(.*?)"/)[1];

        // Adicionar os parâmetros desejados
        var modifiedEmbed = `${url}?utm_source=generator&theme=0`;

        // Exibir o embed alterado em um container
        resultContainer.value = `${modifiedEmbed}`;
    } catch (error) {
        // Lidar com erros durante a extração
        errorDiv.textContent = 'Ocorreu um erro ao extrair o código do iframe do Spotify. Certifique-se de que o código fornecido seja válido.';
        resultContainer.textContent = '';
    }
}

function copySpotifyCodeToClipboard() {
    var resultContainer = document.getElementById('result-spotify-Container');
    var tempInput = document.createElement('input');
    tempInput.value = resultContainer.textContent.trim();
    document.body.appendChild(tempInput);
    tempInput.select();
    document.execCommand('copy');
    document.body.removeChild(tempInput);
    // Criando a div de alerta
    var alertDiv = document.createElement('div');
    alertDiv.classList.add('alert');

    // Criando um parágrafo para a mensagem
    var alertMessage = document.createElement('p');
    alertMessage.textContent = 'URL copiada para a área de transferência!';

    // Adicionando o parágrafo dentro da div de alerta
    alertDiv.appendChild(alertMessage);

    // Adicionando a div de alerta ao corpo do documento
    document.body.appendChild(alertDiv);

    // Removendo a div de alerta após alguns segundos
    setTimeout(function() {
        document.body.removeChild(alertDiv);
    }, 2000); // Remove a div após 2 segundos
}
