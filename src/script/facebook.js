function extractFacebookCode() {
    var inputField = document.getElementById('input-facebook-Field');
    var resultContainer = document.getElementById('result-facebook-Container');
    var errorDiv = document.getElementById('error-facebook');

    var facebookCode = inputField.value.trim();

    if (facebookCode === '') {
        errorDiv.textContent = 'Por favor, insira um código do Facebook.';
        resultContainer.textContent = '';
        return;
    }

    errorDiv.textContent = '';

    try {
        // Extrair a parte específica da URL do Facebook do código fornecido
        var match = facebookCode.match(/href=([^&]+)/);
        var facebookUrl = match ? match[1] : '';

        // Exibir o código ajustado em um container
        resultContainer.value = `https://www.facebook.com/plugins/video.php?height=314&href=${facebookUrl}`;
    } catch (error) {
        // Lidar com erros durante a extração
        errorDiv.textContent = 'Ocorreu um erro ao extrair o código do Facebook. Certifique-se de que o código fornecido seja válido.';
        resultContainer.textContent = '';
    }
}

function copyFacebookCodeToClipboard() {
    var resultContainer = document.getElementById('result-facebook-Container');
    var tempInput = document.createElement('input');
    tempInput.value = resultContainer.textContent.trim();
    document.body.appendChild(tempInput);
    tempInput.select();
    document.execCommand('copy');
    document.body.removeChild(tempInput);
    // Criando a div de alerta
    var alertDiv = document.createElement('div');
    alertDiv.classList.add('alert');

    // Criando um parágrafo para a mensagem
    var alertMessage = document.createElement('p');
    alertMessage.textContent = 'URL copiada para a área de transferência!';

    // Adicionando o parágrafo dentro da div de alerta
    alertDiv.appendChild(alertMessage);

    // Adicionando a div de alerta ao corpo do documento
    document.body.appendChild(alertDiv);

    // Removendo a div de alerta após alguns segundos
    setTimeout(function() {
        document.body.removeChild(alertDiv);
    }, 2000); // Remove a div após 2 segun
}
